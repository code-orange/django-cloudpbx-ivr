from asterisk.agi import *
from django.core.management.base import BaseCommand

from django_cloudpbx_models.django_cloudpbx_models.models import *


class Command(BaseCommand):
    help = "Run this command from Asterisk dialplan using AGI"

    def __init__(self):
        super().__init__()
        self._agi = AGI()
        self._last_input = str()

    def ivr_get_tasks(self, ivr_id=1):
        try:
            ivr_tasks = (
                CusIvr.objects.get(id=ivr_id)
                .cusivrtasks_set.all()
                .order_by("task_order")
            )
        except CusIvrTasks.DoesNotExist:
            ivr_tasks = None
            self._agi.verbose("CLOUDPBX - IVR not found")
            self._agi.hangup()

        return ivr_tasks

    def ivr_run(self, ivr_tasks: list):
        for ivr_task in ivr_tasks:
            if ivr_task.application.name == "stream_file":
                self._agi.stream_file(ivr_task.value)
            elif ivr_task.application.name == "wait_for_digit":
                self._last_input = self._agi.wait_for_digit(ivr_task.value)
            elif ivr_task.application.name == "say_number":
                self._agi.say_number(ivr_task.value)
            elif ivr_task.application.name == "verbose":
                self._agi.verbose(ivr_task.value)
            elif ivr_task.application.name == "hangup":
                self._agi.hangup()
            elif ivr_task.application.name == "internal_switch_ivr":
                if ivr_task.value == "last_input":
                    self.ivr_run(self.ivr_get_tasks(int(self._last_input)))
                else:
                    self.ivr_run(self.ivr_get_tasks(int(ivr_task.value)))
            else:
                self._agi.verbose("CLOUDPBX - application not implemented")
                self._agi.hangup()

        return True

    def handle(self, *args, **options):
        self._agi.answer()

        caller_id = self._agi.env["agi_callerid"]
        self._agi.verbose("call from %s" % caller_id)

        return self.ivr_run(self.ivr_get_tasks())
